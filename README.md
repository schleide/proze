# Explainable and Prompt-Guided Zero-Shot Text Classification
This repository contains the code for reproducing the results reported in the paper "ProZe: Explainable and Prompt-Guided Zero-Shot Text Classification" published in IEEE Internet Computing.

A user-friendly demo is available at: https://proze.tools.eurecom.fr/

## ProZe

ProZe is a text classification approach that leverages knowledge from two sources: prompting pre-trained
language models, as well as querying ConceptNet, a common-sense knowledge base which can
be used to add a layer of explainability to the results. 


## Dependencies

Before running any code in this repo, please install the following dependencies:

    numpy
    pandas
    matplotlib
    nltk
    sklearn
    tqdm
    gensim

## Code Overview

This repository is organized as follows:

### 1. [Common files](https://gitlab.eurecom.fr/schleide/proze/-/tree/master/common_files)
   These are all general files and notebooks for various and generic experiments with ProZe. 


### 2. [Silknow](https://gitlab.eurecom.fr/schleide/proze/-/tree/master/silknow)
   These are all the files and notebooks for the experiments with data from the [SILKNOW Knowledge graph](https://ada.silknow.org).
   The dataset export used here is exported and preprocessed very similarly to the following experiment: [Link](https://github.com/silknow/ZSL-KG-silk).

### 3. [Situation](https://gitlab.eurecom.fr/schleide/proze/-/tree/master/situation)
  These are all the files and notebooks for the experiments based on the "Situation" dataset.

## Cite this work
```
@ARTICLE{9822405,
  author={Harrando, Ismail and Reboud, Alison and Schleider, Thomas and Ehrhart, Thibault and Troncy, Raphael},
  journal={IEEE Internet Computing}, 
  title={ProZe: Explainable and Prompt-Guided Zero-Shot Text Classification}, 
  year={2022},
  volume={},
  number={},
  pages={1-9},
  doi={10.1109/MIC.2022.3187080}}
```
